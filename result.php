<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
global $USER;
CModule::IncludeModule("iblock");

if($type = $_POST['type']){
//    $by='sort';
//    $order='desc';
//    $rsUser = CUser::GetList($by, $order, ['ID' => $USER->GetID()], ['FIELDS' => ['ID', 'PERSONAL_GENDER', 'LOGIN']]);
//    if($arUser = $rsUser->Fetch()){
//        $userInfo = $arUser;
//    }
    $today = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());
    $arData = [];
    switch ($type) {
        case 'round':
            $arData = [
                'IBLOCK_ID' => 64,
                'XML_ID' => $_POST['xml_id'],
                'PROPERTY_VALUES' => [
                    'user_id' => CUser::getID(),
                    'date' => $today,
                    'start_time' => $_POST['startTime'],
                    'time' => $_POST['time'],
                    'level' => $_POST['level'],
                    'shapes_count' => $_POST['shapeCount'],
                    'result' => $_POST['correctly'] === 'true' ? 'Y' : 'N',
                    'group_id' => $_POST['xml_id']
                ],
                'PREVIEW_TEXT' => '',
            ];
            break;
        case 'test':
            $arData = [
                'IBLOCK_ID' => 21,
                'PROPERTY_VALUES' => [
                    "userID" => CUser::getID(),
                    'testID' => 329739,
                    "name" =>  "select_info",
                    "version" => "site version",
                ],
                'PREVIEW_TEXT' => $_POST['progressPercent'],
                'DATE_ACTIVE_FROM' => $today
            ];
            break;
    }

    if($arData) {
        $el = new CIBlockElement;
        $arData['NAME'] = 'Выбор фигур';
        $arData['IBLOCK_SECTION_ID'] = false;
        $arData['ACTIVE'] = "Y";
        $elementId = $el->Add($arData);
    }

}