<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
global $USER;
CModule::IncludeModule("iblock");

header('Content-Type: application/json');
echo json_encode(['first' => 1, 'second' => '2']);
die();