const gridCount = 32;

const config = {
	fileMask: function (shape, color, texture) {
		return `shape_${shape}_${color}${texture ? `_${texture}` : ''}.gif`;
	}
};

function shuffleArray(arr) {
	const result = Array.from(arr);
	result.sort(function () { return Math.random() - 0.5; });
	return result;
}

function getRandomArrayElements(arr, count) {
	if (!((arr || []).length && count))
		return [];
	let result = shuffleArray(arr);
	const iterCount = Math.floor(count / arr.length);
	for (let i = 0; i < iterCount; i++)
		result = result.concat(shuffleArray(arr));
	result = result.slice(0, count);
	return result;
}

function getRandomArrayElem(arr) {
	return shuffleArray(arr)[0];
}

function durationToSec(interval) {
	return Math.floor(interval / 1000);
}

function durationToString(interval, withMs = false) {
	let minutes = Math.floor(interval / 60000);
	let seconds = Math.floor(interval / 1000) - minutes * 60;
	let milliseconds = 0;
	if (minutes < 10) { minutes = "0" + minutes; }
	if (seconds < 10) { seconds = "0" + seconds; }
	if (withMs){
		milliseconds = interval - minutes * 60000 - seconds * 1000;
		if (milliseconds < 10) { milliseconds = "00" + milliseconds; }
		else if (milliseconds < 100) { milliseconds = "0" + milliseconds; }
	}
	const result = `${minutes}:${seconds}` + (withMs ? `.${milliseconds}` : '');
	return interval >= 0 ? result : "00:00";
}

class TestApp {
	constructor() {
		this.auth = this.checkAuth();
		this.allCollors = ['red', 'green', 'blue', 'black', 'yellow'];
		this.allShapes = ['1', '2', '3', '6', '7'];
		this.allTextures = ['1', '2', '3', '4', '4'];
		this.levelCount = 1;
		this.curLevel = null;
		this.levels = [];
		this.shapeCount = shapeCount;
		this.roundCount = roundCount;
	}

	startTest(level) {
		this.levelCount = function () {
			switch (level) {
				case 'first': return 1;
				case 'second': return 2;
				case 'third': return 3;
				case 'fourth': return 4;
			}
		}();
		this.curLevel = new TestLevel(this.levelCount, this.allShapes, this.allCollors, this.allTextures);
		this.levels.push(this.curLevel);
	}

	nextLevel() {
		if (++this.levelCount > 4)
			return false;
		this.curLevel = new TestLevel(this.levelCount, this.allShapes, this.allCollors, this.allTextures);
		this.levels.push(this.curLevel);
		return true;
	}

	checkAuth() {
		let res;
		$.ajax(
			{
				url: "checkAuthorization.php",
				type: "POST",
				success: function (data) {
					if (!data) {
						alert("Необходимо авторизоваться!");
						location.href = '/auth';
					}
					res = data;
				},
				error: function (err) {
					alert("Ошибка сервера!");
					location.href = '/tests';
				}
			}
		);
	}

	get level() {
		return this.curLevel;
	}

	get round() {
		return this.curLevel.round;
	}

	get difficulty() {
		return this.curLevel.difficulty;
	}
}

class TestLevel {
	constructor(level, allShapes, allColors, allTextures) {
		this.level = level;
		[
			this.levelShapes,
			this.levelColors,
			this.levelGroups,
			this.roundFunc,
			this.levelTextures,
		] = this.getLevelOptions(this.level, allShapes, allColors, allTextures);
		this.roundCount = 1;
		this.curRound = new TestRound(this.levelShapes, this.levelColors, this.levelGroups, this.roundFunc, this.levelTextures);
		this.rounds = [this.curRound];
	}

	getLevelOptions(level, shapes, colors, textures) {
		//Количество видов фигур, фигур в группах и цветов в зависимости от сложности
		//shape: кол-во видов фигур на уровне
		//color: кол-во цветов фигур на уровне
		//groups: массив групп фигур(count: фигур в группе; check: группа с искомой фигурой)
		

		const options = function() {
			let shapeCountForGroup = shapeCount;
			function getCount(n) {
				shapeCountForGroup = shapeCountForGroup - n;
				if (shapeCountForGroup < 0) {
					n = n + shapeCountForGroup;
					shapeCountForGroup = 0;
				}
				return n;
			}
			switch (level) {
				//Цвет
				case 1: return {
					shape: 5,
					color: 5,
					groups: [{ count: getCount(1), check: true }, { count: getCount((shapeCount - 1)/2), check: false }, { count: getCount((shapeCount - 1)/2), check: false }],
					roundFunc: function(shapes, colors, groups) {
						const roundShape = getRandomArrayElem(shapes);
						const roundColors = getRandomArrayElements(colors, groups.length);
						const emptyBlocks = Array.from(Array(gridCount - shapeCount).keys()).map(function (j) {
							return {empty: true};
						});
						return shuffleArray(groups.reduce(function (acc, group, i) {
							return acc.concat(Array.from(Array(group.count).keys()).map(function (j) {
								return {
									check: group.check,
									shape: roundShape,
									color: roundColors[i],
									align: getRandomArrayElem(['start', 'flex-end', 'center']),
									fileName: config.fileMask(roundShape, roundColors[i]),
								};
							}));
						}, []).concat(emptyBlocks));
					}
				};
				//Форма
				case 2: return {
					shape: 5,
					color: 5,
					groups: [{ count: getCount(1), check: true }, { count: getCount((shapeCount - 1)/2), check: false }, { count: getCount((shapeCount - 1)/2), check: false }],
					roundFunc: function(shapes, colors, groups) {
						const roundColor = getRandomArrayElem(colors);
						const roundShapes = getRandomArrayElements(shapes, groups.length);
						const emptyBlocks = Array.from(Array(gridCount - shapeCount).keys()).map(function (j) {
							return {empty: true};
						});
						return shuffleArray(groups.reduce(function (acc, group, i) {
							return acc.concat(Array.from(Array(group.count).keys()).map(function (j) {
								return {
									check: group.check,
									shape: roundShapes[i],
									color: roundColor,
									align: getRandomArrayElem(['start', 'flex-end', 'center']),
									fileName: config.fileMask(roundShapes[i], roundColor),
								};
							}));
						}, []).concat(emptyBlocks));
					}
				};
				//Текстура
				case 3: return {
					shape: 5,
					color: 5,
					texture: 5,
					groups: [{ count: shapeCount - 1, check: false }, { count: 1, check: true }],
					roundFunc: function(shapes, colors, groups, textures) {
						const roundShape = getRandomArrayElem(shapes);
						const roundColor = getRandomArrayElem(colors);
						const roundTextures = getRandomArrayElements(textures, groups.length);
						const emptyBlocks = Array.from(Array(gridCount - shapeCount).keys()).map(function (j) {
							return {empty: true};
						});
						return shuffleArray(groups.reduce(function (acc, group, i) {
							return acc.concat(Array.from(Array(group.count).keys()).map(function (j) {
								return {
									check: group.check,
									shape: roundShape,
									texture: roundTextures[i],
									color: roundColor,
									align: getRandomArrayElem(['start', 'flex-end', 'center']),
									fileName: config.fileMask(roundShape, roundColor, roundTextures[i]),
								};
							}));
						}, []).concat(emptyBlocks));
					}
				};
				//Вращение
				case 4: return {
					shape: 5,
					color: 5,
					groups: [{ count: shapeCount - 1, check: false }, { count: 1, check: true }],
					roundFunc: function(shapes, colors, groups) {
						const roundShape = getRandomArrayElem(shapes);
						const roundColor = getRandomArrayElem(colors);
						const cssRotateClass = shuffleArray(['left-rotate', 'right-rotate']);
						const emptyBlocks = Array.from(Array(gridCount - shapeCount).keys()).map(function (j) {
							return {empty: true};
						});
						return shuffleArray(groups.reduce(function (acc, group, i) {
							return acc.concat(Array.from(Array(group.count).keys()).map(function (j) {
								return {
									check: group.check,
									shape: roundShape,
									color: roundColor,
									cssClass: cssRotateClass[+group.check],
									align: getRandomArrayElem(['start', 'flex-end', 'center']),
									fileName: config.fileMask(roundShape, roundColor),
								};
							}));
						}, []).concat(emptyBlocks));
					}
				}
			}
		}();
		return [
			getRandomArrayElements(shapes, options.shape),
			getRandomArrayElements(colors, options.color),
			options.groups,
			options.roundFunc,
			getRandomArrayElements(textures, options.texture),
		];
	}

	nextRound() {
		if (++this.roundCount > roundCount)
			return false;
		this.curRound = new TestRound(this.levelShapes, this.levelColors, this.levelGroups, this.roundFunc, this.levelTextures);
		this.rounds.push(this.curRound);
		return true;
	}

	get difficulty() {
		const difficultyMap = [
			'Цвет',
			'Форма',
			'Текстура',
			'Вращение',
		];
		return difficultyMap[this.level-1];
	}

	get round() {
		return this.curRound
	}
}

class TestRound {
	constructor(shapes, colors, groups, roundFunc, textures) {
		this.shapes = roundFunc(shapes, colors, groups, textures);
		this.result = null;
		//Установить в ручную
		this.startTime = 0;
	}

	selectShape(shapeCheckData) {
		this.result = {
			correctly: shapeCheckData,
			time: this.duration,
			startTime: this.startTime
		};
		return this.result;
	}

	get duration() {
		//5500 Время, пока мигают цифры
		return Date.now() - this.startTime - 5500;
	}
}

$(document).ready(function () {
	//Constants
	const app = new TestApp();
	const xml_id = Date.now();
	let roundTimer;
	const pageElements = {
		playBtn: $('#play'),
		nextBtn: $('#next'),
		levelField: $('#levels'),
		one: $('#one'),
		two: $('#two'),
		three: $('#three'),
		info: $('#info'),
		wrap: $('#wrap'),
		pageFst: $('#page1'),
		pageSec: $('#page2'),
		testInfo: $('#test-info'),
		cancel: $('#cancel')
	};

	//Generators
	const levelGenerator = levelGeneratorFunc();
	let roundGenerator = roundGeneratorFunc();

	function* levelGeneratorFunc() {
		while (app.nextLevel()) {
			yield startLevel();
		}
		cancelTest();
	}

	function* roundGeneratorFunc() {
		do {
			yield startRound();
		} while (app.level.nextRound());
		cancelLevel();
	}

	//Functions
	function startTest() {
		app.startTest(pageElements.levelField.val());
		startLevel();
	}

	function renderScene() {
		pageElements.wrap.hide().empty();
		pageElements.testInfo.hide().empty();
		const htmlContent = app.round.shapes.map(function (shape) {
			return shape.empty ?
				`<div class="empty-item"></div>` :
				`<div data-check="${shape.check}" class="shape-item" style="align-self: ${shape.align}"><div class="shape-img${shape.cssClass ? ` ${shape.cssClass}` : ''}" style="background-image: url(./shapes/${shape.fileName})"></div></div>`;
		}).join('\r\n');
		pageElements.three.delay(1000).fadeIn(500).delay(500).fadeOut(500);
		pageElements.two.delay(2500).fadeIn(500).delay(500).fadeOut(500);
		pageElements.one.delay(4000).fadeIn(500).delay(500).fadeOut(500);
		pageElements.wrap.append(`<div id="scene">${htmlContent}</div>`).delay(5500).fadeIn(300);
		pageElements.testInfo.append(`Уровень: ${app.difficulty}<br/>Раунд: ${app.level.roundCount}<br/>Время: ${app.roundTime || '<div id="rount-time">00:00</div>'}`).delay(500).fadeIn(500);
		roundTimer = setInterval(function () {
			if (roundTime && app.round.duration > roundTime * 1000)
				cancelRound(false);
			pageElements.testInfo.find('#rount-time').text(durationToString(app.round.duration)+(roundTime ? ` / ${roundTime} c` : ''))
		}, 10);
	}

	function startLevel() {
		roundGenerator.next();
	}

	function startRound() {
		renderScene();
		app.round.startTime = Date.now();
		pageElements.wrap.find('.shape-item').on('click', function(e) {
			cancelRound($(e.currentTarget).data('check'));
		});
		return true;
	}

	function cancelRound(shapeCheck) {
		const roundResult = app.round.selectShape(shapeCheck);
		$.post('result.php', {
			...roundResult,
			type: 'round',
			level: app.level.difficulty,
			shapeCount: app.shapeCount,
			xml_id
		});
		clearInterval(roundTimer);
		roundGenerator.next();
	}

	function cancelLevel() {
		roundGenerator = roundGeneratorFunc();
		levelGenerator.next();
	}

	function cancelTest() {
		pageElements.testInfo.hide().empty();
		pageElements.wrap.hide().empty();
		const htmlContent = app.levels.map(function (level) {
			if (level.rounds.length) {
				const averageTime = durationToString(Math.floor(level.rounds.reduce(function (acc, round) {
					acc += round.result.time;
					return acc;
				}, 0) / roundCount), true);
				const roundTable = level.rounds.map(function (round, i) {
					return `<tr class="${round.result.correctly ? 'right' : 'wrong'}"><td>${i + 1}</td><td>${durationToString(round.result.time, true)}</td></tr>`;
				}).join('');
				return `<div class="level-result">Уровень: ${level.difficulty}</br>Кол-во стимулов: ${shapeCount}</br><table><tr class="header"><th>№</th><th>Время попытки</th></tr>${roundTable}<tr><td>Среднее</td><td>${averageTime}</td></tr></table></div>`;
			}
			return null;
		}).join('');
		pageElements.cancel.fadeIn(300);
		pageElements.wrap.append(`<div id="result">${htmlContent}</div>`).fadeIn(300);
		const correctRoundCount = app.levels.reduce(function (acc, level) {
			acc.roundCount = level.rounds.length;
			acc.correctlyRoundCount = level.rounds.filter(function(round){return round.result.correctly}).length;
			return acc;
		}, {roundCount: 0, correctlyRoundCount: 0});
		console.log(correctRoundCount);
		$.post('result.php', {
			type: 'test',
			progressPercent: Math.round(correctRoundCount.correctlyRoundCount / correctRoundCount.roundCount * 100),
			xml_id
		});//TODO options
	}

	//Events
	//Начало теста
	pageElements.playBtn.on('click', function () {
		pageElements.playBtn.fadeOut(500);
		pageElements.info.delay(1000).fadeIn(300);
	});

	//Подтверждение сложности
	pageElements.nextBtn.on('click', function () {
		pageElements.info.fadeOut(500);
		startTest();
	});
	//Конец теста
	pageElements.cancel.find('#cancel-btn').on('click', function(){
		window.location.replace('http://psytest.nstu.ru/tests');
	});
});

