<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Оценка селекции объектов в зависимости от значимости");

CModule::IncludeModule("iblock");
$el = new CIBlockElement;
$settings = [];
$res = CIBlockElement::GetByID(329793);
if (($ar_res = $res->GetNextElement()) && ($ar_res = $ar_res->GetProperties()))
    echo '<script>';
    foreach ($ar_res as $key => $settingObj)
        echo "const $key = ".$settingObj['VALUE'].";";
    echo '</script>';
?>

<link href="style.css" rel="stylesheet">
<link href="time/timeTo.css" type="text/css" rel="stylesheet">
<script src="script.js"></script>
<script src="time/jquery.time-to.min.js"></script>
<div class="content">
    <div class="btn play" id="play">
        Начать тест
    </div>

    <div class="block2" id="info">
        <h1>Оценка селекции объектов в зависимости от значимости</h1>

        <div class="rules" id="page1">
            <img src="36.png" width="40">
            <h2>
                Задание предназначено для определения скоростных характеристик селекции информации
                с оценкой цвета, формы, текстуры либо направления вращения целевого объекта.
            </h2>
            <p id="lev">При предъявлении на экране геометрических фигур, следует как можно быстрее
                найти уникальную фигуру, отличающуюся от всех прочих по одной из характеристик: цветом, формой, текстурой либо направлением вращения
                Старайтесь принимать решение как можно быстрее, не теряя при этом точности определения.
            </p>
        </div>
        <div class="rules2" id="page2">
            <img src="35.png" width="40">
            <h2>Выберите уровень сложности</h2>
            <select name="level" id="levels">
                <option value="first">Цвет</option>
                <option value="second">Форма</option>
                <option value="third">Текстура</option>
                <option value="fourth">Вращение</option>
            </select>
            <p> Первый уровень - найдите фигуру, отличающуюся по цвету</p>
            <p> Второй уровень - найдите фигуру, отличающуюся по форме</p>
            <p> Третий уровень - найдите фигуру, отличающуюся по текстуре</p>
            <p> Четвертый уровень - найдите фигуру, отличающуюся по направлению вращения</p>
            <input class="btn next" id="next" type="submit" value="Далее">
        </div>
    </div>
    <div id="test-info"></div>
    <div class="counter">
        <b id="three">3</b>
        <b id="two">2</b>
        <b id="one">1</b>
    </div>

    <p class="result"></p>

    <input class="next-test" type="button" value="Далее">

    <div id="cancel">
        <h1>Тест пройден</h1>
        <div class="btn" id="cancel-btn">Выход</div>
    </div>
    <div id="wrap"></div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
